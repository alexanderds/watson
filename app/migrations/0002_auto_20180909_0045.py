# Generated by Django 2.1.1 on 2018-09-09 04:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aluno',
            name='id_aluno',
            field=models.IntegerField(unique=True),
        ),
        migrations.AlterField(
            model_name='escola',
            name='id_escola',
            field=models.IntegerField(unique=True),
        ),
        migrations.AlterField(
            model_name='turma',
            name='id_turma',
            field=models.IntegerField(unique=True),
        ),
    ]
