# Generated by Django 2.1.1 on 2018-09-09 07:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_auto_20180909_0045'),
    ]

    operations = [
        migrations.CreateModel(
            name='MediaAluno',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ano', models.CharField(max_length=4)),
                ('media_portugues_dec', models.DecimalField(decimal_places=15, max_digits=18)),
                ('media_portugues_conceito', models.CharField(max_length=50)),
                ('media_matematica_dec', models.DecimalField(decimal_places=15, max_digits=18)),
                ('media_matematica_conceito', models.CharField(max_length=50)),
                ('aluno', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='MediaAluno', to='app.Aluno')),
                ('escola', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='MediaAlunoEscola', to='app.Escola')),
                ('turma', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='MediaAlunoTurma', to='app.Turma')),
            ],
        ),
    ]
