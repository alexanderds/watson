from django.shortcuts import render
import xlrd
import os
from watson import settings
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from decimal import Decimal

from .models import Aluno, Escola,MediaAluno, Turma, DadosEscola, Seduc
from .forms import InputFileForm

# Create your views here.

def uploadfile(request):
    if request.method == "POST":
        data = request.FILES['input_file']
        path = default_storage.save('somename.xlsx', ContentFile(data.read()))
        tmp_file = os.path.join(settings.MEDIA_ROOT, path)
        book = xlrd.open_workbook(tmp_file)
        sheet = book.sheet_by_index(0)
        for i in range(sheet.nrows):
            if i > 0:
                # try:
                if sheet.cell_value(i, 28) != '':
                    aluno = Aluno.objects.get(id_aluno=sheet.cell_value(i, 11))
                    escola = Escola.objects.get(id_escola=sheet.cell_value(i, 5))
                    turma = Turma.objects.get(id_turma=sheet.cell_value(i, 8))
                    clean_media_lp =  str(Decimal(sheet.cell_value(i, 28))).replace('.', '')
                    clean_media_mt =  str(Decimal(sheet.cell_value(i, 32))).replace('.', '')
                    media_lp = clean_media_lp[0:3] +'.' + clean_media_lp[3:]
                    media_mt = clean_media_mt[0:3] +'.' + clean_media_mt[3:]
                    conceito_lp = n_conceito_lp(float(clean_media_lp[0:3]))
                    conceito_mt = n_conceito_mt(float(clean_media_mt[0:3] ))
                    
                    print(sheet.cell_value(i, 28))
                    print(sheet.cell_value(i, 32))
                    print(clean_media_lp)
                    print(clean_media_mt)
                    print(media_lp)
                    print(media_mt)
                    print(conceito_lp)
                    print(conceito_mt)
                    
                    media_aluno = MediaAluno.objects.create(
                        ano=str(sheet.cell_value(i, 0))[:4],
                        aluno=aluno,
                        escola=escola,
                        turma=turma,
                        media_portugues_dec=Decimal(media_lp),
                        media_portugues_conceito=conceito_lp,
                        media_matematica_dec=Decimal(media_mt),
                        media_matematica_conceito=conceito_mt,
                    )
                # except Exception as e:
                #     print(str(sheet.cell_value(i, 11)))
                #     print(str(e))
        os.remove(tmp_file)
    return render(request, 'update_file.html', {'form':InputFileForm})


def n_conceito_lp(media):
    if media < 225.00:
        return 'Nivel 0'
    if media >= 225.00 and media <= 224.00:
        return 'Nivel 1'
    if media >= 225.00 and media <= 249.00:
        return 'Nivel 2'
    if media >= 250.00 and media <= 274.00:
        return 'Nivel 3'
    if media >= 275.00 and media <= 299.00:
        return 'Nivel 4'
    if media >= 300.00 and media <= 324.00:
        return 'Nivel 5'
    if media >= 325.00 and media <= 349.00:
        return 'Nivel 6'
    if media >= 350.00 and media <= 374.00:
        return 'Nivel 7'
    if media >= 375.00 and media <= 400.00:
        return 'Nivel 8'

def n_conceito_mt(media):
    if media < 200.00:
        return 'Nivel 0'
    if media >= 200.00 and media <= 224.00:
        return 'Nivel 1'
    if media >= 225.00 and media <= 249.00:
        return 'Nivel 2'
    if media >= 250.00 and media <= 274.00:
        return 'Nivel 3'
    if media >= 275.00 and media <= 299.00:
        return 'Nivel 4'
    if media >= 300.00 and media <= 324.00:
        return 'Nivel 5'
    if media >= 325.00 and media <= 349.00:
        return 'Nivel 6'
    if media >= 350.00 and media <= 374.00:
        return 'Nivel 7'
    if media >= 375.00 and media <= 399.00:
        return 'Nivel 8'
    if media >= 400.00 and media <= 425.00:
        return 'Nivel 9'


def input_file_escola(request):
    if request.method == "POST":
        data = request.FILES['input_file']
        path = default_storage.save('somename.xlsx', ContentFile(data.read()))
        tmp_file = os.path.join(settings.MEDIA_ROOT, path)
        book = xlrd.open_workbook(tmp_file)
        sheet = book.sheet_by_index(0)
        for i in range(sheet.nrows):
            if i > 0:
                    
                escola = Escola.objects.get(id_escola=sheet.cell_value(i, 2))

                DadosEscola.objects.create(
                    ano=str(sheet.cell_value(i, 0))[:4],
                    id_escola=escola,
                    dependencia=sheet.cell_value(i, 3),
                    localizacao=sheet.cell_value(i, 4),
                    nivel_socioeconomico=sheet.cell_value(i, 7),
                    telhado=sheet.cell_value(i, 8),
                    paredes=sheet.cell_value(i, 9),
                    piso=sheet.cell_value(i, 10),
                    entrada=sheet.cell_value(i, 11),
                    pátio=sheet.cell_value(i, 12),
                    corredores=sheet.cell_value(i, 13),
                    salas_de_aula=sheet.cell_value(i, 14),
                    portas=sheet.cell_value(i, 15),
                    janelas=sheet.cell_value(i, 16),
                    banheiros=sheet.cell_value(i, 17),
                    cozinha=sheet.cell_value(i, 18),
                    instalações_hidráulicas=sheet.cell_value(i, 19),
                    instalações_elétricas=sheet.cell_value(i, 20),
                    salas_iluminadas=sheet.cell_value(i, 21),
                    salas_arejadas=sheet.cell_value(i, 22),
                    controle_entrada_alunos=sheet.cell_value(i, 23),
                    controle_entrada_pessoas=sheet.cell_value(i, 24),
                    vigilância_diurno=sheet.cell_value(i, 25),
                    vigilância_noturno=sheet.cell_value(i, 26),
                    vigilância_finais_de_semana=sheet.cell_value(i, 27),
                    policiamento_furto=sheet.cell_value(i, 28),
                    policiamento_drogas=sheet.cell_value(i, 29),
                    policiamento_externo=sheet.cell_value(i, 30),
                    proteção_contra_incêndio=sheet.cell_value(i, 31),
                    iluminação_externa=sheet.cell_value(i, 32),
                    há_muros_grades=sheet.cell_value(i, 33),
                    segurança_equipamentos_caros=sheet.cell_value(i, 34),
                    portões_trancados_em_aula=sheet.cell_value(i, 35),
                    medida_segurança_externa=sheet.cell_value(i, 36),
                    depredação=sheet.cell_value(i, 37),
                    computadores_para_alunos=sheet.cell_value(i, 38),
                    acesso_internet_para_alunos=sheet.cell_value(i, 39),
                    computadores_para_profressores=sheet.cell_value(i, 40),
                    acesso_internet_para_professores=sheet.cell_value(i, 41),
                    computadores_administrativos=sheet.cell_value(i, 42),
                    fitas_ou_dvd_educativos=sheet.cell_value(i, 43),
                    fitas_ou_dvd_lazer=sheet.cell_value(i, 44),
                    máquina_copiadora=sheet.cell_value(i, 45),
                    impressora=sheet.cell_value(i, 46),
                    retroprojetor=sheet.cell_value(i, 47),
                    projetor_slides=sheet.cell_value(i, 48),
                    videocassete_dvd=sheet.cell_value(i, 49),
                    televisão=sheet.cell_value(i, 50),
                    memeógrafo=sheet.cell_value(i, 51),
                    câmera_fotográfica=sheet.cell_value(i, 52),
                    antena_parabólica=sheet.cell_value(i, 53),
                    internet=sheet.cell_value(i, 54),
                    telefone=sheet.cell_value(i, 55),
                    fax=sheet.cell_value(i, 56),
                    som=sheet.cell_value(i, 57),
                    biblioteca=sheet.cell_value(i, 58),
                    sala_de_leitura=sheet.cell_value(i, 59),
                    quadra=sheet.cell_value(i, 60),
                    lab_informática=sheet.cell_value(i, 61),
                    lab_de_ciências=sheet.cell_value(i, 62),
                    auditório=sheet.cell_value(i, 63),
                    sala_de_música=sheet.cell_value(i, 64),
                    sala_de_artes_plásticas=sheet.cell_value(i, 65),
                    acervo=sheet.cell_value(i, 66),
                    brinquedoteca=sheet.cell_value(i, 67),
                    espaço_estudos=sheet.cell_value(i, 68),
                    pode_emprestar=sheet.cell_value(i, 69),
                    comunidade_pode_usar_emprestar=sheet.cell_value(i, 70),
                    iluminado_e_arejado=sheet.cell_value(i, 71),
                    tem_responsável=sheet.cell_value(i, 72),
                    aluno_pode_empresar=sheet.cell_value(i, 73),
                    professor_pode_empresar=sheet.cell_value(i, 74),
                    comunidade_pode_empresar=sheet.cell_value(i, 75),
                )

        os.remove(tmp_file)
    return render(request, 'update_file.html', {'form':InputFileForm})


def input_file_alter_escola(request):
    if request.method == "POST":
        data = request.FILES['input_file']
        path = default_storage.save('somename.xlsx', ContentFile(data.read()))
        tmp_file = os.path.join(settings.MEDIA_ROOT, path)
        book = xlrd.open_workbook(tmp_file)
        sheet = book.sheet_by_index(0)
        for i in range(sheet.nrows):
            if i > 0:
                    
                escola = Escola.objects.get(id_escola=sheet.cell_value(i, 3))

                dado = DadosEscola.objects.get(ano='2015', id_escola=escola.pk)
                

                pt = sheet.cell_value(i, 57)
                mt = sheet.cell_value(i, 58)
                if sheet.cell_value(i, 57) == '':
                    pt = None
                if sheet.cell_value(i, 58) == '':
                    mt = None

                dado.media_portugues = pt
                dado.media_matematica = mt
                dado.save()

        os.remove(tmp_file)
    return render(request, 'update_file.html', {'form':InputFileForm})

def input_file_seduc(request):
    if request.method == "POST":
        data = request.FILES['input_file']
        path = default_storage.save('somename.xlsx', ContentFile(data.read()))
        tmp_file = os.path.join(settings.MEDIA_ROOT, path)
        book = xlrd.open_workbook(tmp_file)
        sheet = book.sheet_by_index(0)
        for i in range(sheet.nrows):
            if i > 0:
                try:
                    escola = Escola.objects.get(id_escola=sheet.cell_value(i, 3))

                    Ano = str(sheet.cell_value(i, 0)).replace('.0', '')
                    IdEscola = escola
                    IdTurma = sheet.cell_value(i, 7)
                    if IdTurma == "NULL":
                        IdTurma = None
                    IdAluno = sheet.cell_value(i, 8)
                    if IdAluno == "NULL":
                        IdAluno = None
                    TotalFaltas = sheet.cell_value(i, 10)
                    if TotalFaltas == "NULL":
                        TotalFaltas = None
                    TotalPresenca = sheet.cell_value(i, 11)
                    if TotalPresenca == "NULL":
                        TotalPresenca = None

                    Seduc.objects.create(
                        Ano =Ano,
                        IdEscola =IdEscola,
                        IdTurma =IdTurma,
                        IdAluno =IdAluno,
                        TotalFaltas =TotalFaltas,
                        TotalPresenca =TotalPresenca,
                    )


                except Exception as a:
                    print(a)


        os.remove(tmp_file)
    return render(request, 'update_file.html', {'form':InputFileForm})