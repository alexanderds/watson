from django import forms

from crispy_forms.bootstrap import AppendedText, Div
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, HTML

class InputFileForm(forms.Form):
    """InputFileForm definition."""

    input_file = forms.FileField()

    def __init__(self, *args, **kwargs):
        super(InputFileForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.layout = Layout(
            Div(
                Div('input_file', css_class='col-md-6'),
            Div(Submit('salvar', 'Subir', css_class='btn btn-success'),
                HTML("<a class='btn btn-danger' href='#'>Cancelar</a>"),
                css_class='col-md-12'), css_class="col-md-12")
            )

