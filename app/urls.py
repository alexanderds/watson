from django.urls import path
from . import views

urlpatterns = [
    path('', views.uploadfile, name='uploadfile'),
    path('escola', views.input_file_escola, name='input_file_escola'),
    path('alter_escola', views.input_file_alter_escola, name='alter_escola'),
    path('seduc', views.input_file_seduc, name='seduc'),
]