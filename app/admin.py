from django.contrib import admin
from .models import Aluno, Escola, MediaAluno, Turma, DadosEscola
# Register your models here.
admin.site.register(Aluno)
admin.site.register(Escola)
admin.site.register(MediaAluno)
admin.site.register(Turma)
admin.site.register(DadosEscola)